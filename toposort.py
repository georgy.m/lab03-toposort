'''Meshcheryakov Georgy
This module implements topological sorting algorithm based on Kahn's approach.
'''
from collections import defaultdict
from copy import copy


def parse_graph(s: str):
    '''Parses a graph from a textual description into a dictonary.
    A graph is submitted to the functions using the following syntax:
        nodeA <- nodeB + nodeC + ... + nodeZ
        nodeC <- nodeB
        etc
    which means that nodes nodeB-nodeZ send an incoming arrow into nodeA,
    node B also has an arrow into nodeC and so on. Here, all nodes but
    nodeA and nodeC are exogenous. If no "<-" symbol is present or lhs
    is empty, then the node is added still added to the graph (although with
    no arrows going anywhere).
    Keyword arguments:
        s -- A string description of a graph.
    Returns:
        An adjacency dictionary.
    '''
    r = defaultdict(set)  # type: defaultdict[str, set]
    for line in s.splitlines():
        line = line.replace(' ', '').strip()
        if line:
            items = line.split('<-')
            lv, rv = items[0], items[1:]
            if rv:
                rv = rv[0].split('+')
                for v in rv:
                    # Unfortunately, pylint and mypy are not smart enough
                    # to understand that the following line has a purpose,
                    # so an uglly approach is chosen.
                    # r[v]
                    if v not in r:
                        r[v] = set()
            r[lv].update(rv)
    return r


def topological_sort(graph: dict):
    '''Performs topological sorting of the graph's vertices.
    Keyword arguments:
        graph -- An adjacency dictionary as returned by parse_graph.
    Returns:
        A list containt graph vertices in a topologically ascending order.
        None if graph is not a DAG.
    '''
    graph = copy(graph)
    full_endogenous = [node for node, ins in graph.items() if not ins]
    res = list()
    while full_endogenous:
        node = full_endogenous.pop()
        res.append(node)
        for n, ins in graph.items():
            try:
                ins.remove(node)
                if not ins:
                    full_endogenous.append(n)
            except KeyError:
                continue
    for _, nodes in graph.items():
        if nodes:
            return None
    return res
