from itertools import permutations
import unittest
import toposort

_graph_empty = ''
_graph_single = 'a'
_graph_disjoints = 'a\nb\nc'
_graph = '''
a <- b + c
b <- c
e
'''


class TestGraphParsing(unittest.TestCase):
    def read(self, desc: str, corr_dict: dict):
        d = toposort.parse_graph(desc)
        for n, nodes in d.items():
            assert n in corr_dict
            assert corr_dict[n] == nodes
        for n, nodes in corr_dict.items():
            assert n in d
            assert d[n] == nodes

    def test_empty(self):
        corr_dict = {}
        self.read(_graph_empty, corr_dict)

    def test_single(self):
        corr_dict = {'a': set()}
        self.read(_graph_single, corr_dict)

    def test_disjoints(self):
        corr_dict = {'a': set(), 'b': set(), 'c': set()}
        self.read(_graph_disjoints, corr_dict)

    def test_graph(self):
        corr_dict = {'a': {'b', 'c'}, 'b': {'c'}, 'c': set(), 'e': set()}
        self.read(_graph, corr_dict)


class TestTopoSort(unittest.TestCase):
    def sort(self, desc: str, corr_sorts):
        graph = toposort.parse_graph(desc)
        sort = toposort.topological_sort(graph)
        assert sort in corr_sorts

    def test_empty(self):
        corr_sort = [[]]
        self.sort(_graph_empty, corr_sort)

    def test_single(self):
        corr_sort = [['a']]
        self.sort(_graph_single, corr_sort)

    def test_disjoints(self):
        corr_sort = list(map(list, list(permutations(['a', 'b', 'c']))))
        self.sort(_graph_disjoints, corr_sort)

    def test_graph(self):
        corr_sort = [['e', 'c', 'b', 'a']]
        self.sort(_graph, corr_sort)


if __name__ == '__main__':
    unittest.main()
